/// <reference types="cypress" />

describe('API Testing', () => {
  it('Should passes', () => {
    cy.request('/users/2').then((response) => {
      cy.log(JSON.stringify(response.headers));
      cy.log(JSON.stringify(response.body));
    });
  });

  it('Should validate headers', () => {
    cy.request('/users/2').as('user');
    cy.get('@user')
      .its('headers')
      .its('content-type')
      .should('include', 'application/json');

    cy.get('@user')
      .its('headers')
      .its('connection')
      .should('include', 'keep-alive');
  });

  it('Should validate status code', () => {
    // Positive scenario
    cy.request('/users/2').as('existingUser');
    cy.get('@existingUser').its('status').should('equal', 200);
    // Negative scenario - IMPORTANT TO PUT failOnStatusCode parameter !!!!
    cy.request({ url: '/users/non-exist', failOnStatusCode: false }).as(
      'nonExistingUser',
    );
    cy.get('@nonExistingUser').its('status').should('equal', 404);
  });

  it('Should validate GET REQUEST', () => {
    cy.request('/users/2').as('user');
    cy.get('@user').then((res) => {
      cy.log(JSON.stringify(res.body));
      expect(res.body.data.id).equal(2);
      expect(res.body.data.email).contains('janet.weaver@reqres.in');

      const userID = res.body.data.id;
      expect(userID).equal(2);
    });
  });

  it('Should validate POST REQUEST', () => {
    cy.request({
      url: '/login',
      method: 'POST',
      body: {
        email: 'eve.holt@reqres.in',
        password: 'cityslicka',
      },
    }).as('loginRequest');
    cy.get('@loginRequest').its('status').should('equal', 200);
    cy.get('@loginRequest').then((res) => {
      expect(res.body.token).to.equal('QpwL5tke4Pnpja7X4');
    });
  });

  it('Should validate POST REQUEST - ERROR', () => {
    cy.request({
      url: '/login',
      method: 'POST',
      failOnStatusCode: false,
      body: { email: 'eve.holt@reqres.in' },
    }).as('loginRequest');
    cy.get('@loginRequest').its('status').should('equal', 400);
    cy.get('@loginRequest').then((res) => {
      expect(res.body.error).to.equal('Missing password');
    });
  });

  it('Should validate DELETE REQUEST', () => {
    cy.request({
      url: '/users/2',
      method: 'DELETE'
    }).as('deleteUser');
    cy.get('@deleteUser').its('status').should('equal', 204);
  });

  it('Should validate PUT REQUEST', () => {
    cy.request({
      url: '/users/2',
      method: 'PUT',
      body: { name: 'name-updated' },
    }).as('putRequest');
    cy.get('@putRequest').its('status').should('equal', 200);
    cy.get('@putRequest').then((res) => {
      expect(res.body.name).to.equal('name-updated');
    });
  });

  it('Should validate Auth tokens', () => {
    cy.request({
      url: '/users/2',
      method: 'PUT',
      body: { name: 'name-updated' },
      auth: { bearer: 'my-token-value' },
    }).as('putRequest');
    
    cy.get('@putRequest').its('status').should('equal', 200);
    cy.get('@putRequest').then((res) => {
      expect(res.body.name).to.equal('name-updated');
    });
  });
});
