[![Language - Javascript](https://img.shields.io/badge/Javascript-F2DB3F?style=for-the-badge&logo=javascript&logoColor=white)](https://)
[![Testing - API](https://img.shields.io/badge/API-orange?style=for-the-badge&logo=api&logoColor=white)](https://)



<br/>
<br/>
<div align="center">
    <img src="./logo.jpg" alt="Logo" width="30%">
    <br/>
    <br/>
    <h1 align="center"><strong>Formation Cypress - Testing API</strong></h1>
    <br />
    <p align="center">Docker - Page Object - Actions - CI - Visual Regression</p>
</div>
  
 
<br/>
<br/>
